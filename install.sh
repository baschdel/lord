#!/bin/bash

bin_directory=~/.local/bin
daemon_directory=~/.config/lord/daemons
import_directory=~/.config/lord/import

logger_lua=~/.local/share/lord/logger.lua

files="lord lord_daemon lordmon lordctl"

mkdir -p "$bin_directory"
mkdir -p "$daemon_directory"
mkdir -p "$import_directory"
	
for name in `echo "$files" | tr ' ' '\n'`; do
	echo "Installing $name ..."
	cp -f "$name.sh" "$bin_directory/$name"
	chmod +x "$bin_directory/$name"
done

# install logproc
echo "Installing logproc.lua"
mkdir -p "$(dirname "$logger_lua")"
cp -f logproc.lua "$logger_lua"

echo "Done!"
