#!/bin/bash

echo "lordmon.sh is deprecated, please use 'lord monitor' instead!" >&2

monitor_directory="/run/user/$UID/lord/monitors"

[ -d "$monitor_directory" ] || mkdir -p "$monitor_directory"

fifo_name="$monitor_directory/lord_monitor_$(uuidgen)"
mkfifo "$fifo_name"

end() {
	rm -f "$fifo_name"
}

trap end EXIT

while true; do
	cat "$fifo_name"
done
