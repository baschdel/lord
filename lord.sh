#!/bin/bash

##########################
## CONFIG ##############
#######################

log_directory=~/.local/share/lord/daemon_logs
lord_log_file=~/.local/share/lord/lord.log
daemon_directory=~/.config/lord/daemons
import_directory=~/.config/lord/import
pidfile_directory=/run/user/$UID/lord/pidfiles
lock_directory=/run/user/$UID/lord/locks
monitor_directory=/run/user/$UID/lord/monitors

# The loacation of the default logrotation script
logger_lua=~/.local/share/lord/logger.lua

#messages
declare -A messages
messages[lockfile_open_fail]="Could not open file descriptor for lock!"
messages[lockfile_lock_fail]="Could not acquire lock for daemon!"

messages[start_already_running]="Deamon is already running!"
messages[start_success]="Daemon started."
messages[start_disabled]="Daemon is disabled!"
messages[start_no_startscript]="No start script for this daemon!"

messages[stop_not_running]="Daemon is not running."
messages[stop_success]="Deamon stopped."
messages[stop_sigint]="Asking nicely to stop …"
messages[stop_sigterm]="Asking to stop …"
messages[stop_sigkill]="Asking the OS to clean the mess up …"
##########################
## CODE ################
#######################

[ -d "$log_directory" ] || mkdir -p "$log_directory"
[ -d "$pidfile_directory" ] || mkdir -p "$pidfile_directory"
[ -d "$lock_directory" ] || mkdir -p "$lock_directory"

log_uuid=$(uuidgen)
log_daemon="unknown"


writemonitor() {
	if [ -d "$monitor_directory" ]; then
		for fifo in $(ls $monitor_directory); do
			[ -p "$monitor_directory/$fifo" ] && echo "$1" > "$monitor_directory/$fifo"
		done
	fi
}

# log(domain, message_id)
# domains:
# - error
# - info
# - progress
log() {
	echo "$(date '+%Y-%m-%d %H-%M-%S') $log_uuid $log_daemon $1 $2" >> $lord_log_file
	log_message="${messages[$2]}"
	[ ! -z "$log_message" ] && echo "[$log_daemon][$1] $log_message" >&2
	[ "$2" == "start_success" ] && writemonitor "start $log_daemon"
	[ "$2" == "stop_success" ] && writemonitor "stop $log_daemon"
}

usedaemon() {
	log_daemon=$1
	exec 100>"$lock_directory/$1" || (log error lockfile_open_fail; exit 100)
}

lockdaemon() {
	flock -w 2 100 || (log error lockfile_lock_fail; exit 101)
}

#logmon(daemon)
logmon() {
	if [ ! -f "$logger_lua" ]
	then
		# fall back to a normal logfile if no logger was detected
		1>> "$log_directory/$1"
	else
		lua "$logger_lua" "$log_directory/%Y-%m-%d_$1" '%H:%M:%S| '
	fi
	log info daemon_exited
	stop $1 2>/dev/null > /dev/null
}

# daemon(daemon)
daemon() {
	lockdaemon "$1"
	("$daemon_directory/$1" 2>&1 & echo "$!" > "$pidfile_directory/$1") | logmon $1 &
}

get_pid() {
	if [ -f "$pidfile_directory/$1" ]
	then
		PID="$(head -n 1 "$pidfile_directory/$1")"
		if [ -d "/proc/$PID" ]
		then
			echo $PID
			return 0
		fi
	fi
	if [ -x "$import_directory/$1" ]
	then
		"$import_directory/$1" && return 0
	else
		if [ -f "$import_directory/$1.pgrep" ]
		then
			pgrep -U $UID -x --oldest "$(cat "$import_directory/$1.pgrep")" && return 0
		else
			return 1
		fi
	fi
}

isrunning() {
	[ ! -z "$(get_pid "$1")" ]
}

# recursive ps, lists all children of a process, including grandchildren of any degree
# rps(pid,treesub?,prefix?)
rps() {
	echo "$3$1"
	for child in $(pgrep -P "$1")
	do
		rps $child "$2" "$3$2"
	done
}

start() {
	if [ -x "$daemon_directory/$1" ]
	then
		if isrunning "$1"
		then
			log error start_already_running
			exit 3
		else
			daemon "$1" &
			log info start_success
		fi
	else
		if [ -f "$daemon_directory/$1" ]
		then
			log error start_disabled
			exit 2
		else
			log error start_no_startscript
			exit 1
		fi
	fi
}

# sendsignal(signal, pids)
sendsignal() {
	for process in "$2"
	do
		kill -s $1 $process 2> /dev/null
	done
}

#waitforquit(pid, interval, maxintervals)
waitforquit() {
	while kill -0 "$1" 2> /dev/null
	do
		((i=i+1))
		[ $i -gt $3 ] && return 1
		sleep $2
	done
	return 0
}

stop() {
	pid=$(get_pid "$1")
	if [ -z "$pid" ]
	then
		log error stop_not_running
		return 1
	fi
	pids=$(rps "$pid")
	log progress stop_sigint
	sendsignal SIGINT "$pids"
	waitforquit $pid .2 15
	log progress stop_sigterm
	sendsignal SIGTERM "$pids"
	waitforquit $pid .2 15
	log progress stop_sigkill
	sendsignal SIGKILL "$pids"
	waitforquit $pid .2 50
	if [ -f "$pidfile_directory/$1" ]
	then
		lockdaemon "$1"
		rm -f "$pidfile_directory/$1"
	fi
	log info stop_success
	return 0
}

status() {
	if [ -z "$1" ]
	then
		for daemon in $(ls $daemon_directory)
		do
			echo "$daemon	$(status "$daemon")"
		done
	else
		pid=$(get_pid "$1")
		if [ -z "$pid" ]
		then
			echo "stopped"
		else
			if [ -f "$pidfile_directory/$1" ]
			then
				echo "running ($pid)"
			else
				echo "running ($pid) (imported)"
			fi
		fi
	fi
}

ddb_status() {
	if [ -z "$1" -o "$1" = "." ]
	then
		for daemon in $(ls $daemon_directory)
		do
			ddb_status "$daemon" "$2"
		done
	else
		service_prefix="lord_daemon."
		if [ ! -z "$2" ]
		then
			service_prefix="$2"
		fi
		name="$service_prefix$1"
		echo "> $name type service"
		pid=$(get_pid "$1")
		if [ -z "$pid" ]
		then
			echo "> $name active no"
			echo "u $name pid"
			echo "u $name imported"
		else
			echo "> $name active yes"
			echo "> $name pid $pid"
			if [ -f "$pidfile_directory/$1" ]
			then
				echo "u $name imported"
			else
				echo "> $name imported yes"
			fi
		fi
	fi
}

monitor_end() {
	rm -f "$monitor_fifo_name"
}

monitor() {
	[ -d "$monitor_directory" ] || mkdir -p "$monitor_directory"
	monitor_fifo_name="$monitor_directory/lord_monitor_$(uuidgen)"
	mkfifo "$monitor_fifo_name"

	trap monitor_end EXIT
	
	while true; do
		cat "$monitor_fifo_name"
	done
}

ddb_monitor() {
	$0 monitor | while read -r event name; do
		ddb_status "$name" "$1"
	done
}

ddb_subscribe_all() {
	service_prefix="lord_daemon."
	if [ ! -z "$1" ]
	then
		service_prefix="$1"
	fi
	for daemon in $(ls "$daemon_directory")
	do
		echo "+ $service_prefix$daemon"
	done
}

daemon_ps() {
	pid=$(get_pid "$1")
	if [ ! -z "$pid" ]
	then
		rps "$pid" $2
	else
		echo "Deamon is not running"
		exit 1
	fi
}

daemon_pstree() {
	pid=$(get_pid "$1")
	if [ ! -z "$pid" ]
	then
		pstree -T -l "$pid"
	else
		echo "Deamon is not running"
		exit 1
	fi
}

ddb_send_commands() {
	ddb_monitor "$1" &
	while true; do
		ddb_subscribe_all "$1"
		ddb_status . "$1"
		sleep 300
	done
}

ddb_receive_commands() {
	service_prefix="lord_daemon."
	if [ ! -z "$1" ]
	then
		service_prefix="$1"
	fi
	lua -e "local prefix='$service_prefix' local function unprefix(name) if not name then return end if name:sub(1,#prefix) == prefix then return name:sub(#prefix+1):match('^[A-Za-z0-9%-]+$') end end while true do local i = io.read() if not i then return end local name,command = i:match('^s (.-) (.+)$') name = unprefix(name) if name then if command == 'start' or command == 'stop' or command == 'restart'  then os.execute(\"lord \"..command..\" '\"..name..\"'\") end end end" 
}

case "$1" in
	start)
		usedaemon "$2"
		log command start
		start "$2"
	;;
	stop)
		usedaemon "$2"
		log command stop
		stop "$2"
	;;
	stopall)
		log command stopall
		for daemon in $(ls "$pidfile_directory")
		do
			usedaemon "$daemon"
			stop "$daemon"
		done
	;;
	restart)
		if isrunning "$2"
		then
			if [ -x "$daemon_directory/$2" ]
			then
				usedaemon "$2"
				log command restart
				echo "Stopping daemon …"
				stop "$2"
				sleep .1
				echo "Starting daemon …"
				start "$2"
			else
				if [ -f "$daemon_directory/$2" ]
				then
					echo "Daemon is disabled, not restarting!"
					exit 11
				else
					echo "Daemon has no start script, not restarting!"
					exit 12
				fi
			fi
		else
			echo "Daemon is not running, not restarting!"
			exit 13
		fi
	;;
	restartall)
		log command restartall
		for daemon in $(ls "$daemon_directory")
		do
			if isrunning "$daemon"
			then
				if [ -x "$daemon_directory/$daemon" ]
				then
					echo "Restarting $daemon …"
					usedaemon "$daemon"
					if stop "$daemon"
					then
						sleep .1
						start "$daemon"
					else
						echo "Failed to stop $daemon, not restarting"
					fi
				fi
			fi
		done
	;;
	status)
		status "$2"
	;;
	monitor)
		monitor
	;;
	ps)
		daemon_ps "$2" " |"
	;;
	rps)
		daemon_ps "$2"
	;;
	pstree)
		daemon_pstree "$2"
	;;
	listall)
		ls "$daemon_directory" | cat
	;;
	ddb_status)
		ddb_status "$2" "$3"
	;;
	ddb_monitor)
		ddb_monitor "$2"
	;;
	ddb_subscribe_all)
		ddb_subscribe_all "$2"
	;;
	ddb_send_commands)
		ddb_send_commands "$2"
	;;
	ddb_receive_commands)
		ddb_receive_commands "$2"
	;;
	ddb_agent)
		ddb_send_commands "$2"&
		ddb_receive_commands "$2"
	;;
*)
cat <<EOF
Version: 2021-08-21.0

Usage:
	lord start <daemon>    - start a daemon
	lord stop <daemon>     - stop a daemon
	lord restart <daemon>  - restart daemon
	lord status [<daemon>] - query the status of one or all daemons
	lord rps <daemon>      - list process ids as flat list
	lord ps <daemon>       - list process ids as a simple tree
	lord pstree <daemon>   - runs pstree on the root pid
	lord monitor    - prints when daemons are started/stopped
	lord listall    - lists all daemons, running and stopped
	lord stopall    - stops all daemons, useful for updating
	lord restartall - restarts all daemons, useful for updating
	lord help       - print this message

NOTE: restart intentionally fails if the service is not running
NOTE: for the rps subcommand the first pid is always the pid of the daemon script
NOTE: the status command only includes files that have a startscript in the all view
NOTE: the monitor script does not work for imported processes (Merge requests welcome!)

	lord ddb_status <daemon|.> [<prefix>] - status for ddb integration
	lord ddb_monitor [<prefix>]           - monitor for ddb integration
	lord ddb_subscribe_all [<prefix>]     - generates subscribe commands
	lord ddb_send_commands [<prefix>]     - status, monitor, subscribe
	lord ddb_receive_commands [<prefix>]  - command handler for ddb
	lord ddb_agent [<prefix>]             - sends and receives ddv commands

NOTE: default prefix is "lord_daemon."
NOTE: use ddb_agent with the ddb_connector script to integrate with ddb

EOF
esac
