local filename_format,log_date_format = ...

if not filename_format then
	print("Usage:")
	print("\tlogproc.lua <filename_format> [<log_date_format>]")
	print("")
	print("\t<filename_format> the format of the filename, basically a filepath that is also a date template")
	print("\t example: './logs/%Y-%m-%d_%H_testlog'")
	print("\t<log_date_format> the format of the date prepended to every logged line")
	print("\t default: '%H:%M:%S| '")
	return
end

local lastupdate = "never"
local output_file = io.stdout

local log_date_format = log_date_format or "%H:%M:%S| "
local current_log_prefix = "[sometime]"

local lasttime = 0

function check_logfile()
	local now = os.date(filename_format)
	if lastupdate ~= now then
		if output_file ~= io.stdout then
			output_file:flush()
			output_file:close()
		end
		output_file = io.open(now,"a")
		lastupdate = now
	end
end

while true do
	-- read line
	local line = io.read()
	if not line then break end
	-- only call the checkfile function once a second
	local now = os.time()
	if lasttime ~= now then
		current_log_prefix = os.date(log_date_format)
		check_logfile()
		lasttime = now
	end
	-- write line to the logfile
	output_file:write(current_log_prefix..line.."\n")
	output_file:flush()
end
