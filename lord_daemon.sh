#!/bin/bash

COMMAND_FIFO=/run/user/$UID/lord/command_fifo
LORD=lord

rm -f "$COMMAND_FIFO"
mkfifo "$COMMAND_FIFO"

end() {
	rm -f "$COMMAND_FIFO"
}

trap end EXIT

while true; do
	cat "$COMMAND_FIFO" | \
	while read CMD; do
		echo "[command] $CMD"
		COMMAND=""
		SERVIVCE=""
		[[ "$CMD" =~ ^start ]] && COMMAND="start" && SERVIVCE="${CMD:6}"
		[[ "$CMD" =~ ^restart ]] && COMMAND="restart" && SERVIVCE="${CMD:8}"
		[[ "$CMD" =~ ^stop ]] && COMMAND="stop" && SERVIVCE="${CMD:5}"
		[[ "$SERVIVCE" =~ ^[a-zA-Z0-9-]+$ ]] && [ ! -z "$COMMAND" ] && $LORD "$COMMAND" "$SERVIVCE" &
	done
done

