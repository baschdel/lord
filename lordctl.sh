#!/bin/bash

COMMAND_FIFO=/run/user/$UID/lord/command_fifo
if [ ! -p "$COMMAND_FIFO" ]
then
	echo "lord_daemon is not running"
	exit 1
fi

if [[ "$1" =~ ^(start|restart|stop)$ ]]
then
	if [[ "$2" =~ ^[a-zA-Z0-9-]+$ ]]
	then
		echo "$1 $2" > "$COMMAND_FIFO"
		echo "Command sent."
	else
		echo "Service name must only constist of lowercase letters, numbers and '-'"
		exit 3;
	fi
else
	echo "Usage:"
	echo "	lordctl can instruct a lord_daemon to start, restart and stop services"
	echo ""
	echo "	$0 start <service>"
	echo "	$0 restart <service>"
	echo "	$0 stop <service>"
	exit 2;
fi
