# Lord of Daemons

Lord is a simple Daemon manager written in bash and lua that can start and stop processes running in the background and log their output

## What is …
### lord.sh

lord.sh (after installing simply type lord) is the core script that implements all the logic for starting ad logging processes and identifying process children to stop all of them wehen requested.

Runngin `lord help` will give you the following output:
```
Version: 2021-08-21.0

Usage:
	lord start <daemon>    - start a daemon
	lord stop <daemon>     - stop a daemon
	lord restart <daemon>  - restart daemon
	lord status [<daemon>] - query the status of one or all daemons
	lord rps <daemon>      - list process ids as flat list
	lord ps <daemon>       - list process ids as a simple tree
	lord pstree <daemon>   - runs pstree on the root pid
	lord listall    - lists all daemons, running and stopped
	lord stopall    - stops all daemons, useful for updating
	lord restartall - restarts all daemons, useful for updating
	lord help       - print this message

NOTE: restart intentionally fails if the service is not running
NOTE: for the rps subcommand the first pid is always the pid of the daemon script
NOTE: the status command only includes files that have a startscript in the all view

	lord ddb_status <daemon|.> [<prefix>] - status for ddb integration
	lord ddb_monitor [<prefix>]           - monitor for ddb integration
	lord ddb_subscribe_all [<prefix>]     - generates subscribe commands
	lord ddb_send_commands [<prefix>]     - status, monitor, subscribe
	lord ddb_receive_commands [<prefix>]  - command handler for ddb
	lord ddb_agent [<prefix>]             - sends and receives ddv commands

NOTE: default prefix is "lord_daemon."
NOTE: use ddb_agent with the ddb_connector script to integrate with ddb
```

### logproc.lua
logproc.lua is a script that thakes a stram of log output on stdin and routes it to logfiles with timestamps both in the filenames and the log output.

It is used by lord.sh

### lordmon.sh

The lordmon.sh script is deprecated the functionality has moved to the 'lord monitor' command.

lordmon.sh (after installing simply type lordmon) is a script that gives you a line of output whenever a daemon managed by lord gets started or stopped (this excludes imported daemons).

It does this by lacing a fifo in the monitors directory and simply printing everything that gets written to it to stdout, the script takes no arguments

### lord_daemon.sh
lord_daemon.sh (after installing simply type lord_daemon) is a wrapper script around lord.sh that listens for start, stop and restart commands on a fifo file (/run/user/$UID/lord/command_fifo) and then executes them. It is useful for when you don't want to have parent child relations between the process invokig lord and the processes that get started by lord, i.e. when you want to start a service as part of another service or systemd hook.

lord_daemon thake no arguments

### lordctl.sh 
lordctl.sh (after installing simply type lordctl) is a simple script that makes it easy to send command to a lord_daemon

```
Usage:
	lordctl can instruct a lord_daemon to start, restart and stop services

	/home/baschdel/.local/bin/lordctl start <service>
	/home/baschdel/.local/bin/lordctl restart <service>
	/home/baschdel/.local/bin/lordctl stop <service>

```

## Installing

To install lord you first have to clone this repository.

If you have the bin directory of your useraccount at ~/.local/bin then you don't have to configure anything before installation, if you have your binarys somewhere else edit the install.sh script and change the bin_directory variable to the desired install directory.

Run the install.sh script as the user you wat to install lord for and the only thing missing is the daemon start and import files. (Since I don't really know what exactly you want to manage with lord you have to create those yourself)

### Updating

If you want to update lord simply run the install pocedure again.

### Uninstalling

Currently there is no easy way to uninstall lord, but all files created and used by lord are documented in the files section, simply remove them.

## FAQ

### How do I create a new daemon?
you create a new executable (script) file in the daemons directory inside your configured config directory, this script will be run when the daemon is started and will receive a sigterm when the daemon should stop.

### How do I configure automatic restarting?
You have to write the restarting logic yourself

### Why does my daemon get reported as stopped when its running?
It is probably detaching itself from the shell it was started on (daemon mode), try to disable that.

### My daemon gets started by something that is not this script, can I still use lord to manage it?
Yes, if you create an executable script in the import directory that returns the pid of the process to be considered for status querys and stop commands
(shortcut: if you place a file called &ltname&gt.pgrep in the import directory the file content will be passed to $(pgrep -U $UID -x --oldest &ltfilecontent&gt))

### How do I configure dependencies?
This is out of scope for this system. Script it yourself.

### I just made a daemon file and lord start says it is disabled.
That is not a question, but you probably forgot to chmod +x your service file.

### What are all those ddb_* commands about?
They are for integration with the [ddb object key value store](https://gitlab.com/baschdel/ddb/) to easily integrate with custom system ui and management scripts.

## Files

lord puts files/uses files in the following places

### ~/.config/lord/daemons/
This directory contains executable files that lord can start as services (I recommend you to put shell scripts there, but any executable will do) if the file is not executable then lord will fail to start the daemon saying that it is disabled.

Filname equals service name, don't use file extensions

### ~/.config/lord/import/
This directory contains files that can tell lord the pids of processes, even if they were not started by lord, if they are executables the same rules apply as for the daemons directory. The scripts get called by lord wehenever a mapping from name to pid is needed and there is no pidfile. If a simple `pgrep <process_name>` solves your problem put the process_name part as the only content in a file called `<daemon_name>.pgrep`

### ~/.local/share/lord/daemon_logs/
This is where lord puts the logfiles for the daemons

### ~/.local/share/lord/lord.log
This is where lord keeps its own logfile

### /run/user/$UID/lord/pidfiles/
This is the directory where lord keep its pidfiles to deterime the process id if a service wa tarted by lord.

### /run/user/$UID/lord/locks
Lord keeps some empty files here hat it lock to prevent race conditions

### /run/user/$UID/lord/monitors
You can put a fifo there and lord will write service states into them.

the states will be in the format:
```
start <service>
stop <service>
```

### /run/user/$UID/lord/command_fifo
Lord_daemon places its fifo here that accept commands in a line based format

You can manually submit commands by simply `echo restart somedaemon > /run/user/$UID/lord/command_fifo`

## DDB objects

Using the ddb_agent or the other ddb commands lord exports one object for each service using a prefix immedeantly followed by the service name as the object name.

The objects may have the following fields:
- active - contains yes or no
- pid - set when active, contains the primary pid
- imported - set to yes when active but not started by lord
